<%-- 
    Document   : books
    Created on : 19-Dec-2018, 21:48:41
    Author     : crist
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:pageTemplate pageTitle="CLC's Books">
    <h1>Library Books</h1>
    <c:forEach var="book" items="${books}" varStatus="status">
    <div class="row">
        <div class="col-md-2">
            ${book.nume}
        </div>
        <div class="col-md-2">
            ${book.autor}
        </div>
        <div class="col-md-2">
            ${book.an}
        </div>
        <h5>Copies: ${numberOfCopies}</h5>
    </div>
    </c:forEach>
        
</t:pageTemplate>
