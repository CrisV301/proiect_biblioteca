/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.biblioteca.ejb;

import com.biblioteca.common.BookDetails;
import com.biblioteca.entity.Books;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author crist
 */
@Stateless
public class BookBean {

    private static final Logger LOG = Logger.getLogger(BookBean.class.getName());

    @PersistenceContext
    private EntityManager em;
    public List<BookDetails> getAllBooks(){
        LOG.info("getAllBooks");
        try{
            Query query =em.createQuery("SELECT b FROM Books b");
            List<Books> books=(List<Books>) query.getResultList();
            return copyBooksToDetails(books);
        }
        catch(Exception ex){
            throw new EJBException(ex);
        }
    }
    private List<BookDetails> copyBooksToDetails(List<Books> books){
        List<BookDetails> detailsList=new ArrayList<>();
        for(Books book:books){
            BookDetails bookDetails=new BookDetails(book.getId(),
            book.getCopie1(),
            book.getCopie2(),
            book.getCopie3(),        
            book.getAn(),
            book.getAutor(),
            book.getNume(),
            book.getNrCarte());
            detailsList.add(bookDetails);
        }
        return detailsList;
    }
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
