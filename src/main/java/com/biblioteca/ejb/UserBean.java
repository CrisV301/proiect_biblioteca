/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.biblioteca.ejb;

import com.biblioteca.common.BookDetails;
import com.biblioteca.common.UserDetails;
import com.biblioteca.entity.Books;
import com.biblioteca.entity.Useri;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author crist
 */
@Stateless
public class UserBean {
    private static final Logger LOG = Logger.getLogger(UserBean.class.getName());

    @PersistenceContext
    private EntityManager em;
    public List<UserDetails> getAllUsers(){
        LOG.info("getAllUsers");
        try{
            Query query =em.createQuery("SELECT u FROM Useri u");
            List<Useri> users=(List<Useri>) query.getResultList();
            return copyUsersToDetails(users);
        }
        catch(Exception ex){
            throw new EJBException(ex);
        }
    }
    private List<UserDetails> copyUsersToDetails(List<Useri> users){
        List<UserDetails> detailsList=new ArrayList<>();
        for(Useri user:users){
            UserDetails userDetails=new UserDetails(user.getId(),
            user.getCarte1(),
            user.getCarte2(),
            user.getCarte3(),
            user.getUsername(),
            user.getPassword());
            detailsList.add(userDetails);
        }
        return detailsList;
    }
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
