/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.biblioteca.common;

/**
 *
 * @author crist
 */
public class BookDetails implements java.io.Serializable{
    private Integer id,copie1,copie2,copie3,an;
    private String autor,nume,nr_carte;
    
    public BookDetails(Integer id,Integer copie1,Integer copie2,Integer copie3,Integer an,String autor,String nume,String nr_carte){
        this.id=id;
        this.copie1=copie1;
        this.copie2=copie2;
        this.copie3=copie3;
        this.an=an;
        this.autor=autor;
        this.nume=nume;
        this.nr_carte=nr_carte;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCopie1() {
        return copie1;
    }

    public void setCopie1(Integer copie1) {
        this.copie1 = copie1;
    }

    public Integer getCopie2() {
        return copie2;
    }

    public void setCopie2(Integer copie2) {
        this.copie2 = copie2;
    }

    public Integer getCopie3() {
        return copie3;
    }

    public void setCopie3(Integer copie3) {
        this.copie3 = copie3;
    }

    public Integer getAn() {
        return an;
    }

    public void setAn(Integer an) {
        this.an = an;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getNr_carte() {
        return nr_carte;
    }

    public void setNr_carte(String nr_carte) {
        this.nr_carte = nr_carte;
    }
    
}
