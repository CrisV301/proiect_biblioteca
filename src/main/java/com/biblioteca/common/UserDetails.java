/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.biblioteca.common;

/**
 *
 * @author crist
 */
public class UserDetails implements java.io.Serializable{
    private Integer id,carte1,carte2,carte3;
    private String username,password;
    public UserDetails(Integer id,Integer carte1,Integer carte2,Integer carte3,String username,String password){
        this.id=id;
        this.carte1=carte1;
        this.carte2=carte2;
        this.carte3=carte3;
        this.username=username;
        this.password=password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCarte1() {
        return carte1;
    }

    public void setCarte1(Integer carte1) {
        this.carte1 = carte1;
    }

    public Integer getCarte2() {
        return carte2;
    }

    public void setCarte2(Integer carte2) {
        this.carte2 = carte2;
    }

    public Integer getCarte3() {
        return carte3;
    }

    public void setCarte3(Integer carte3) {
        this.carte3 = carte3;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
